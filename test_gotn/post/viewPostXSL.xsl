<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet  [
	<!ENTITY nbsp   "&#160;">
	<!ENTITY copy   "&#169;">
	<!ENTITY reg    "&#174;">
	<!ENTITY trade  "&#8482;">
	<!ENTITY mdash  "&#8212;">
	<!ENTITY ldquo  "&#8220;">
	<!ENTITY rdquo  "&#8221;"> 
	<!ENTITY pound  "&#163;">
	<!ENTITY yen    "&#165;">
	<!ENTITY euro   "&#8364;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
<xsl:output method="html" encoding="utf-8"/>
<xsl:template match="/">

<html>
	<head><script type="text/javascript" src="../test_gotn/post/jquery_shorten-master/src/jquery-latest.js"></script>
	<script type="text/javascript" src="../test_gotn/post/jquery_shorten-master/src/jquery.shorten.js"></script>
	<link href="../test_gotn/post/viewPostCSS.css" rel="stylesheet" type="text/css" />
	
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	</head>
	<body>
    <script type="text/javascript">
            function ran_col() { //function name
                var color = '#'; // hexadecimal starting symbol
                var letters = ['000000','FF0000','00FF00','0000FF','FFFF00','00FFFF','FF00FF','C0C0C0']; //Set your colors here
                color += letters[Math.floor(Math.random() * letters.length)];
                document.getElementById('posts').style.background = color; // Setting the random color on your div element.
            }
        </script>
    
    
		<xsl:for-each select="posts/post"> <!-- 23-01-2014 -->
 			<xsl:sort select="substring(date,7,4)" order="descending"/> <!-- year  -->
            <xsl:sort select="substring(date,4,2)" order="descending"/> <!-- month   -->    
  		  	<xsl:sort select="substring(date,1,2)" order="descending"/> <!-- day -->
    		    <!-- 22:59:01-->
            <xsl:sort select="substring(time,1,2)" order="descending"/>
            <xsl:sort select="substring(time,4,2)" order="descending"/>
            <xsl:sort select="substring(time,7,2)" order="descending"/>
            <div id="posts">
        	
        	<div class="user"><xsl:value-of select="user"/></div>
        <div class="subject"><xsl:value-of select="subject"/></div>
        <div class="comment"><pre style="white-space: pre;"><div class="message"><xsl:value-of select="message"/></div></pre></div>
     
       <xsl:variable name="unit_id">
  <xsl:value-of select="tag" /> 
  </xsl:variable>
  
  
<a class="one" href="../test_gotn/post/viewPostByTag.php?tag={$unit_id}">
  <div class="tag">tag : <xsl:value-of select="tag"/></div></a>
      

        
        <div class="date">Date : <xsl:value-of select="date"/> Time <xsl:value-of select="time"/></div>
        </div>
        </xsl:for-each>
        
        
		
        
        
        <script language="javascript">
$(document).ready(function() {
	
	$(".comment").shorten();
	
	$(".comment-small").shorten({showChars: 10});

 });
</script>
    </body>
</html>
</xsl:template>
</xsl:stylesheet>