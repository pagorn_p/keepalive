<?php
header('Content-Type: text/html; charset=utf-8');
$title_post=stripslashes($_POST[title_post]);
$user_post=$_POST[user_post];
$message_post=stripslashes($_POST[message_post]);
$tag_post=stripslashes($_POST[tag_post]);
$date_post=$_POST[date_post];
$time_post=$_POST[time_post];


//str_replace('\\\'', '\'', str_replace('\\"', '"', $var);); เป็นวิธีที่ดีเหมือนกัน



if ($title_post=="" or $message_post=="" or $user_post=="") {
	echo "<h3>ERROR : กรอกข้อมูลไม่ครบ</h3>"."<a href='#' onclick='history.back();"
				."return false;'>กลับไป</a>";
				exit();
}

$xml = new DOMDocument();
$xml->preserveWhiteSpace = false;
$xml->formatOutput = true;
$xml -> load('postList.xml');


$xml_posts = $xml->getElementsByTagName('posts')->item(0);


$xml_post = $xml->createElement("post");



$xml_user = $xml->createElement("user"); //สร้าง tag user
$xml_user_text = $xml->createTextNode($user_post); //สร้าง text ขึ้นมา
$xml_user->appendChild($xml_user_text); //ใส่ text ลง tag

$xml_sub = $xml->createElement("subject");
$xml_sub_text = $xml->createTextNode($title_post);
$xml_sub->appendChild($xml_sub_text);

$xml_mess = $xml->createElement("message");
//$xml_mess_text = $xml->createTextNode($message_post);
$xml_mess_text = $xml->createCDATASection($message_post);
$xml_mess->appendChild($xml_mess_text);




$xml_date = $xml->createElement("date");
$xml_date_text = $xml->createTextNode($date_post);
$xml_date->appendChild($xml_date_text);

$xml_time = $xml->createElement("time");
$xml_time_text = $xml->createTextNode($time_post);
$xml_time->appendChild($xml_time_text);

$xml_tag = $xml->createElement("tag");
$xml_tag_text =$xml->createTextNode($tag_post);
$xml_tag->appendChild($xml_tag_text);

$xml_post->appendChild($xml_user);
$xml_post->appendChild($xml_sub);
$xml_post->appendChild($xml_mess);
$xml_post->appendChild($xml_date);
$xml_post->appendChild($xml_tag);
$xml_post->appendChild($xml_time);


$xml_posts->appendChild($xml_post);


//echo "<xmp>". $xml->saveXML() ."</xmp>";

$xml->save("postList.xml") or die("Error");

echo "<h2>Post เรียบร้อย</h2>";
header('Location: ../../index.php#post');
//echo $message_post;
?>