<?php
header('Content-Type: text/html; charset=utf-8');
$xml = new DOMDocument;
$xml->load('postList.xml');
$xsl = new DOMDocument;
$xsl->load('viewPostXSL.xsl');

// Configure the transformer
$proc = new XSLTProcessor;
$proc->importStyleSheet($xsl); // attach the xsl rules

echo $proc->transformToXML($xml);

?>
