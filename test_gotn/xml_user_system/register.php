<?php
$errors = array();
if(isset($_POST['login'])){
	$username = preg_replace('/[^A-Za-z0-9_]/', '', $_POST['username']);
	$email = $_POST['email'];
	$password = $_POST['password'];
	$c_password = $_POST['c_password'];
	$about = $_POST['about'];
	$fullname=$_POST['fullname'];
	
	if(file_exists('users/' . $username . '.xml')){
		$errors[] = '<font color="#DC143C">Username already exists</font>';
	}
	if($fullname == ''){
		$errors[] = '<font color="#DC143C">Fullname is blank</font>';
	}
	if($username == ''){
		$errors[] = '<font color="#DC143C">Username is blank</font>';
	}
	if($email == ''){
		$errors[] = '<font color="#DC143C">Email is blank</font>';
	}
	if($password == '' || $c_password == ''){
		$errors[] = '<font color="#DC143C">Passwords are blank</font>';
	}
	if($password != $c_password){
		$errors[] = '<font color="#DC143C">Passwords do not match</font>';
	}
	if(count($errors) == 0){
		$xml = new SimpleXMLElement('<user></user>');
		$xml->addChild('password', md5($password));
		$xml->addChild('email', $email);
		$xml->asXML('users/' . $username . '.xml');
		
		$xml2 = new DOMDocument();
		$xml2->preserveWhiteSpace = false;
		$xml2->formatOutput=true;
		$xml2 -> load('userList.xml');
		
		$xml2_users = $xml2->getElementsByTagName('users')->item(0);
		
		$xml2_user = $xml2->createElement("user");
		

		
		$xml2_fullname = $xml2->createElement('fullname');
		$xml2_fullname_text = $xml2->createTextNode($fullname);
		$xml2_fullname->appendChild($xml2_fullname_text);
		
		$xml2_username = $xml2->createElement('username');
		$xml2_username_text = $xml2->createTextNode($username);
		$xml2_username->appendChild($xml2_username_text);
		
		$xml2_about = $xml2->createElement('aboutMe');
		$xml2_about_text = $xml2->createTextNode($about);
		$xml2_about->appendChild($xml2_about_text);
		
		$xml2_user->appendChild($xml2_fullname);
		$xml2_user->appendChild($xml2_username);
		$xml2_user->appendChild($xml2_about);
		
		$xml2_users->appendChild($xml2_user);

		$xml2->save("userList.xml") or die("Error");
		
		header('Location: http://keepalive.datasynz.com/test_gotn/xml_user_system/login.php');
		die;
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Register</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css">
body {
	background: transparent;
}
</style>
</head>
<body>
<h1>:: Register</h1>
	<form method="post" action="">
		<?php
		if(count($errors) > 0){
			echo '<ul>';
			foreach($errors as $e){
				echo '<li>' . $e . '</li>';
			}
			echo '</ul>';
		}
		?>
        <TABLE CELLSPACING="2">
        <tr><td><b>Full Name</b></td><td><input type="text" name="fullname" size="30"/></td></tr>
    <TR> 
      <TD><b>Username</b></TD>
      <td> <input type="text" name="username" size="30"/></td>
      </TR>
        <tr>
        <td>
		<b>Email</b></td>
        <td> <input type="text" name="email" size="30" /></td>
        </tr>
        <tr>
		<td><b>Password</b></td><td><input type="password" name="password" size="30" /></td></tr>
        <tr>
		<td><b>Confirm Password</b></td><td><input type="password" name="c_password" size="30" /></td>
        </tr>
        <TR> 
      <TD><B>About Me</B></TD>
      <TD><TEXTAREA NAME="about" COLS="31" ROWS="5" id="about"></TEXTAREA></TD>
    </TR>
		<tr><TD>&nbsp;</TD>
        <td><input type="submit" name="login" value="Create Account" /> <INPUT Type="button" VALUE="Back" onClick="history.go(-1);return true;"></td>
	</form>
</body>
</html>