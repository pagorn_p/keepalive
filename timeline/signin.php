<?php
$error = false;
if(isset($_POST['login2'])){
	$username = preg_replace('/[^A-Za-z0-9_]/', '', $_POST['username']);
	$password = md5($_POST['password']);
	if(file_exists('../test_gotn/xml_user_system/users/' . $username . '.xml')){
		$xml = new SimpleXMLElement('../test_gotn/xml_user_system/users/' . $username . '.xml', 0, true);
		if($password == $xml->password){
			session_start();
			$_SESSION['username'] = $username;
			header('Location: profile.php');
			die;
		}
	}
	$error = true;
}

session_start();
if(file_exists('../test_gotn/xml_user_system/users/' . $_SESSION['username'] . '.xml')){
	header('Location: profile.php');
	die;
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<html>
	<head>
		<title>KEEPALIVE_SIGNIN</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<!--[if lte IE 8]><script src="css/ie/html5shiv.js"></script><![endif]-->
		<script src="js/jquery.min.js"></script>
		<script src="js/jquery.dropotron.min.js"></script>
		<script src="js/skel.min.js"></script>
		<script src="js/skel-layers.min.js"></script>
		<script src="js/init.js"></script>
		<noscript>
			<link rel="stylesheet" href="css/skel.css" />
			<link rel="stylesheet" href="css/style.css" />
		</noscript>
		<!--[if lte IE 8]><link rel="stylesheet" href="css/ie/v8.css" /><![endif]-->
        
        <link href="css/freelancer.css" rel="stylesheet">
         <link href="bootstrap-social.css" rel="stylesheet" >
        <link href="css/bootstrap.min.css" rel="stylesheet">
         <script src="js/bootstrap.min.js"></script>
    	<script src="js/freelancer.js"></script>
	</head>
	<body class="homepage">
		<!-- Header -->
			<div id="header-wrapper">
				<div id="header" class="container">
					
					<!-- Logo -->
						<h1 id="logo"><a href="index.php">KeepAlive</a></h1>
					
					<!-- Nav -->
						<nav id="nav">
							<ul>
							<li><a href="profile.php">Profile</a></li>
								<li><a href="signup.php">Sign Up</a></li>
								<li  class="break"><a href="signin.php">Sign In</a></li>
								<li><a href="#">About Us</a></li>
							</ul>
						</nav>
					
				</div>

				<!-- Hero -->
					<section id="hero" class="container">
                    <img class="img-responsive" src="images/profile.png" alt="">
						<header>
							<h2>Sign in</h2>
       					</header>
					</section>
                    
            <div id="footer" class="container">
					<div class="row">
						<section class="12u 12u">
							<form method="post" action="">
								<div class="row 100%">
									<div class="12u">
									 	<?php
											if($error){
												echo 'Invalid username and/or password';
											}
										?>
										<!--<label style="text-align: left; color: #FFFFFF;">Username</label> -->
										<input name="username" placeholder="Username" type="text" required autofocus/>
						
                                    </div>
									<div class="12u">
										<!--<label style="text-align: left; color: #FFFFFF; margin-top: 15px;">Password</label> -->
										<input name="password" placeholder="Password" type="password" required/>
									</div>
								</div>
                               <a href="#">Forgot Password?</a>
								<div class="row 50%">
									<div class="12u">
										<ul class="actions">
											<li style="margin-top: 15px; auto"><input type="submit" value="Login" name="login2" /></li>
										</ul>
									</div>
								</div>
							
                          
              </div>
                            </form>
                           </section>
					</div>
				</div>
				
							
                       
                            </form>
                           </section>
					</div> </center>
				</div>
            
              
               
             
            </div>
          </div>
       

			
			

	</body>
</html>