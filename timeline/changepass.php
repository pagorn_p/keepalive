<?php
header('Content-Type: text/html; charset=utf-8');
session_start();
if(!file_exists('../test_gotn/xml_user_system/users/' . $_SESSION['username'] . '.xml')){
	header('Location: signin.php');
	die;
}
$error = false;
if(isset($_POST['change'])){
	$old = md5($_POST['o_password']);
	$new = md5($_POST['n_password']);
	$c_new = md5($_POST['c_n_password']);
	$xml = new SimpleXMLElement('../test_gotn/xml_user_system/users/' . $_SESSION['username'] . '.xml', 0, true);
	if($old == $xml->password){
		if($new == $c_new){
			$xml->password = $new;
			$xml->asXML('../test_gotn/xml_user_system/users/' . $_SESSION['username'] . '.xml');
			header('Location: ../test_gotn/xml_user_system/logout.php');
			die;
		}
	}
	$error = true;
}
$ssn=$_SESSION['username'];
?>
<!DOCTYPE HTML>
<html>
	<head>
		<title>KEEPALIVE</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<!--[if lte IE 8]><script src="css/ie/html5shiv.js"></script><![endif]-->
		<script src="js/jquery.min.js"></script>
		<script src="js/jquery.dropotron.min.js"></script>
		<script src="js/skel.min.js"></script>
		<script src="js/skel-layers.min.js"></script>
		<script src="js/init.js"></script>
		<noscript>
			<link rel="stylesheet" href="css/skel.css" />
			<link rel="stylesheet" href="css/style.css" />
		</noscript>
		<!--[if lte IE 8]><link rel="stylesheet" href="css/ie/v8.css" /><![endif]-->
        
        <link href="css/freelancer.css" rel="stylesheet">
        <link href="css/bootstrap.min.css" rel="stylesheet">
         <script src="js/bootstrap.min.js"></script>
    <script src="js/freelancer.js"></script>
	</head>
	<body class="homepage">
		<!-- Header -->
			<div id="header-wrapper">
				<div id="header" class="container">
					
					<!-- Logo -->
						<h1 id="logo"><a href="index.php">KeepAlive</a></h1>
					
					<!-- Nav -->
						<nav id="nav">
							<ul>
								<li><a href="index.php">HOME</a></li>
								<li><a href="profile.php">PROFILE</a></li>
                                <li class="break"></li>
								<li><a href="signup.php">SIGN UP</a></li>
								<li><a href="signin.php">SIGN IN</a></li>
							</ul>
						</nav>

				</div>

				<!-- Hero -->
					<section id="hero" class="container">
                    <img class="img-responsive" src="images/profile.png" alt="">
						<header>
							<h2><?php echo $_SESSION['username']; ?>'s Profile</h2>
                            <h4>Change Password</h4>
  						</header>
       					
                        <!-- ตรงนี้ป่าว? -->
                        <form method="post" action="">
		<?php 
		if($error){
			echo '<p>Some of the passwords do not match</p>';
		}
		?>
		<p>Old password <input type="password" name="o_password" /></p>
		<p>New password <input type="password" name="n_password" /></p>
		<p>Confirm new password <input type="password" name="c_n_password" /></p>
		<p><input style="color:#000" type="submit" name="change" value="Change Password" /></p>
	</form>
					</section>
			</div>

			
		
		<!-- Footer -->
			

	</body>
</html>