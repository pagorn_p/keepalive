<?php
	

header('Content-Type: text/html; charset=utf-8');
session_start();
if(!file_exists('../test_gotn/xml_user_system/users/' . $_SESSION['username'] . '.xml')){
	header('Location: signin.php');
	die;
}
$uname=$_SESSION['username'];
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>KEEPALIVE_TELLSTORY</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<!--[if lte IE 8]><script src="css/ie/html5shiv.js"></script><![endif]-->
		<script src="js/jquery.min.js"></script>
		<script src="js/jquery.dropotron.min.js"></script>
		<script src="js/skel.min.js"></script>
		<script src="js/skel-layers.min.js"></script>
		<script src="js/init.js"></script>
		<noscript>
			<link rel="stylesheet" href="css/skel.css" />
			<link rel="stylesheet" href="css/style.css" />
		</noscript>
		<!--[if lte IE 8]><link rel="stylesheet" href="css/ie/v8.css" /><![endif]-->
        
        <link href="css/freelancer.css" rel="stylesheet">
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="bootstrap-social.css" rel="stylesheet" >

         <script src="js/bootstrap.min.js"></script>
    		<script src="js/freelancer.js"></script>
	</head>
	<body class="homepage">
		<!-- Header -->
			<div id="header-wrapper">
				<div id="header" class="container">
					
					<!-- Logo -->
						<h1 id="logo"><a href="index.php">KeepAlive</a></h1>
					
					<!-- Nav -->
						<nav id="nav">
							<ul>
							<li><a href="profile.php">Profile</a></li>
								<li><a href="signup.php">Sign Up</a></li>
								<li  class="break"><a href="signin.php">Sign In</a></li>
								<li><a href="#">About Us</a></li>
							</ul>
						</nav>

				</div>

				<!-- Hero -->
					<section id="hero" class="container">
                    <img class="img-responsive" src="images/profile.png" alt="">
						<header>
							<h2>KeepAlive</h2>
                            <p>Tell Your Story</p>
						</header>
					</section>
                    <!-- tell story -->
                    <div id="footer" class="container">
                
					<div class="row">
						<section class="12u">
							<form method="post" action="../test_gotn/post/addPost2.php">
								<div class="row 50%">
									<div class="12u">
										<input name="title_post" placeholder="Title" type="text" />
									</div>
                                
									<div class="12u">
										<textarea name="message_post" placeholder="Tell your story"></textarea>
									</div>
									
                                    <div class="12u">
										<input name="tag_post" placeholder="Tag" type="text" />
 										<?php date_default_timezone_set("Asia/Bangkok"); ?>


                                        <input type="hidden" name="date_post" value="<?=date("d-m-Y")?>"/>
      <input type="hidden" name="time_post" value="<?=date("H:i:s")?>"/>
      <input type="hidden" name="user_post" value="<?php echo $uname; ?>"/>
									</div>
                                    
									<div class="12u">
										<ul class="actions">
											<li><input type="submit" value="Keep it"/></li>
										</ul>
									</div>
								
                                
									
								
                                </form>
                                </section>
						</div>
            </div>


	</body>
</html>