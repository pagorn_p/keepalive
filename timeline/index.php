
<!DOCTYPE HTML>
<html>
	<head>
		<title>KEEPALIVE</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<!--[if lte IE 8]><script src="css/ie/html5shiv.js"></script><![endif]-->
		<script src="js/jquery.min.js"></script>
		<script src="js/jquery.dropotron.min.js"></script>
		<script src="js/skel.min.js"></script>
		<script src="js/skel-layers.min.js"></script>
		<script src="js/init.js"></script>
		<noscript>
			<link rel="stylesheet" href="css/skel.css" />
			<link rel="stylesheet" href="css/style.css" />
		</noscript>
		<!--[if lte IE 8]><link rel="stylesheet" href="css/ie/v8.css" /><![endif]-->
        
        <link href="css/freelancer.css" rel="stylesheet">
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="bootstrap-social.css" rel="stylesheet" >

         <script src="js/bootstrap.min.js"></script>
    		<script src="js/freelancer.js"></script>
	</head>
	<body class="homepage">
		<!-- Header -->
			<div id="header-wrapper">
				<div id="header" class="container">
					
					<!-- Logo -->
						<h1 id="logo"><a href="index.php">KeepAlive</a></h1>
					
					<!-- Nav -->
						<nav id="nav">
							<ul>
								<li><a href="profile.php">Profile</a></li>
								<li><a href="signup.php">Sign Up</a></li>
								<li  class="break"><a href="signin.php">Sign In</a></li>
								<li><a href="#">About Us</a></li>
							</ul>
						</nav>
				</div>

				<!-- Hero -->
					<section id="hero" class="container">
                    <img class="img-responsive" src="images/profile.png" alt="">
                     
						<header>
							<h2>KeepAlive</h2>
                            <p>KEEP story ALIVE forever</p>
                             <ul class="actions">
							<li><a href="tellstory.php" class="button">Tell Your Story</a></li>
							</ul>
					
						</header>
       					 <ul class="actions"><form method="get" action="../test_gotn/post/search.php">
                         	<li><input type="text" style="height: 4em;" placeholder="Tag" name="search"/></li>
							<li><input class="button"  type="submit" value="Search"/></li></form>
                            
                       </ul>
                       </section>
                
			</div>
            
			<a name="post"></a>
			<?php
				header('Content-Type: text/html; charset=utf-8');
				$xml = new DOMDocument;
				$xml->load('../test_gotn/post/postList.xml');
				$xsl = new DOMDocument;
				$xsl->load('../test_gotn/post/viewPostXSL.xsl');
				
				// Configure the transformer
				$proc = new XSLTProcessor;
				$proc->importStyleSheet($xsl); // attach the xsl rules
				
				echo $proc->transformToXML($xml);
				
				?>

				<div id="copyright" style="background-color:#1A1C29">
					<ul>
                    	<li>322275  XML TOWARDS WEB TECHNOLOGIES AND APPLICATIONS sec.1</li></ul>
                        <ul class="menu">
                        	<li>Lect. Dr. Chitsutha Soomlek</li>
                    </ul>
                    <ul class="menu">
                    	<li>553020007-5 Warintorn Phusomsai</li>
                    	<li>553020281-5 Pagorn Petchnukulkait</li>
                    </ul>
                    <ul class="menu">
						<li>&copy; AimGotn's Group. All rights reserved.</li>
                        <li>Design by: <a href="http://html5up.net">HTML5 UP</a></li>
					</ul>
				</div>
                
			</div>

	</body>
</html>