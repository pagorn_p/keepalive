<?php
$errors = array();
if(isset($_POST['login'])){
        $username = preg_replace('/[^A-Za-z0-9_]/', '', $_POST['username']);
        $email = $_POST['email'];
        $password = $_POST['password'];
        $c_password = $_POST['c_password'];
        $about = $_POST['about'];
        $fullname=$_POST['fullname'];
        
        if(file_exists('../test_gotn/xml_user_system/users/' . $username . '.xml')){
                $errors[] = '<font color="#FFFFFF">Username already exists</font>';
        }
        if($fullname == ''){
                $errors[] = '<font color="#FFFFFF">Fullname is blank</font>';
        }
        if($username == ''){
                $errors[] = '<font color="#FFFFFF">Username is blank</font>';
        }
        if($email == ''){
                $errors[] = '<font color="#FFFFFF">Email is blank</font>';
        }
        if($password == '' || $c_password == ''){
                $errors[] = '<font color="#FFFFFF">Passwords are blank</font>';
        }
        if($password != $c_password){
                $errors[] = '<font color="#FFFFFF">Passwords do not match</font>';
        }
        if(count($errors) == 0){
                $xml = new SimpleXMLElement('<user></user>');
                $xml->addChild('password', md5($password));
                $xml->addChild('email', $email);
                $xml->asXML('../test_gotn/xml_user_system/users/' . $username . '.xml');
                
                $xml2 = new DOMDocument();
                $xml2->preserveWhiteSpace = false;
                $xml2->formatOutput=true;
                $xml2 -> load('../test_gotn/xml_user_system/userList.xml');
                
                $xml2_users = $xml2->getElementsByTagName('users')->item(0);
                
                $xml2_user = $xml2->createElement("user");
                
                $xml2_user_attr = $xml2->createAttribute('id');
                $xml2_user_attr->value = ($username);
                $xml2_user->appendChild($xml2_user_attr);
                
                $xml2_fullname = $xml2->createElement('fullname');
                $xml2_fullname_text = $xml2->createTextNode($fullname);
                $xml2_fullname->appendChild($xml2_fullname_text);
                
                $xml2_username = $xml2->createElement('username');
                $xml2_username_text = $xml2->createTextNode($username);
                $xml2_username->appendChild($xml2_username_text);
                
                $xml2_about = $xml2->createElement('aboutMe');
                $xml2_about_text = $xml2->createTextNode($about);
                $xml2_about->appendChild($xml2_about_text);
                
                $xml2_user->appendChild($xml2_fullname);
                $xml2_user->appendChild($xml2_username);
                $xml2_user->appendChild($xml2_about);
                
                $xml2_users->appendChild($xml2_user);

                $xml2->save("../test_gotn/xml_user_system/userList.xml") or die("Error");
				
				

                header('Location: http://keepalive.datasynz.com/timeline/signin.php');
                die;
        }
}



?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>KEEPALIVE_TELLSTORY</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<!--[if lte IE 8]><script src="css/ie/html5shiv.js"></script><![endif]-->
		<script src="js/jquery.min.js"></script>
		<script src="js/jquery.dropotron.min.js"></script>
		<script src="js/skel.min.js"></script>
		<script src="js/skel-layers.min.js"></script>
		<script src="js/init.js"></script>
		<noscript>
			<link rel="stylesheet" href="css/skel.css" />
			<link rel="stylesheet" href="css/style.css" />
		</noscript>
		<!--[if lte IE 8]><link rel="stylesheet" href="css/ie/v8.css" /><![endif]-->
        
        <link href="css/freelancer.css" rel="stylesheet">
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="bootstrap-social.css" rel="stylesheet" >

         <script src="js/bootstrap.min.js"></script>
    		<script src="js/freelancer.js"></script>
	</head>
	<body class="homepage">
		<!-- Header -->
			<div id="header-wrapper">
				<div id="header" class="container">
					
					<!-- Logo -->
						<h1 id="logo"><a href="index.php">KeepAlive</a></h1>
					
					<!-- Nav -->
						<nav id="nav">
							<ul>
							<li><a href="profile.php">Profile</a></li>
                                <li><a href="signup.php">Sign Up</a></li>
                                <li  class="break"><a href="signin.php">Sign In</a></li>
                                <li><a href="#">About Us</a></li>
							</ul>
						</nav>

				</div>

				<!-- Hero -->
					<section id="hero" class="container">
                    <img class="img-responsive" src="images/profile.png" alt="">
						<header>
							<h2>KeepAlive</h2>
                            <p>Tell Your Story</p>
						</header>
					</section>
                    <!--tell story-->
                    <div id="footer" class="container">
                
					<div class="row">
						<section class="12u">
							
                            
                            <form method="post" action="">
		<?php
		if(count($errors) > 0){
			echo '<ul>';
			foreach($errors as $e){
				echo '<li>' . $e . '</li>';
			}
			echo '</ul>';
		}
		?>
        <TABLE CELLSPACING="2">
        <tr><td></td><td><label>Fullname</label><input type="text" name="fullname" size="30" required/></td></tr>
    <TR> 
      <TD></TD>
      <td> <label>Username</label><input type="text" name="username" size="30" required/></td>
      </TR>
        <tr>
        <td>
		</td>
        <td><label>Email</label><input type="text" name="email" size="30" required/></td>
        </tr>
        <tr>
		<td></td><td><label>Password</label><input type="password" name="password" size="30" required/></td></tr>
        <tr>
		<td></td><td><label>Confirm Password</label><input type="password" name="c_password" size="30" required/></td>
        </tr>
        <TR> 
      <TD></TD>
      <TD><label>About Me</label><TEXTAREA NAME="about" COLS="31" ROWS="5" id="about"></TEXTAREA></TD>
    </TR>
		<tr><TD>&nbsp;</TD>
        <br >
        <td><input type="submit" name="login" value="Create Account" /> <INPUT Type="button" VALUE="Back" onClick="history.go(-1);return true;"></td>
	</form>
                            
                                </section>
						</div>
            </div>


	</body>
</html>